# MobileNetSSD vs YoloV3

Applicazione android di computer vision basata sulla libreria OpenCV (3.4.6) che permette il riconoscimento di oggetti su immagini caricate dalla galleria.
L'app confronta due algoritmi di computer vision come YoloV3 e MobileNetSSD e da la possibilità all'utente di votare quale dei due meccanismi ha funzionato meglio.
Viene calcolato il tempo di esecuzione di ciascuno dei due algoritmi e gli oggetti identificati verranno identificati tramite un quagrato colorato.
La scelta dell'utente viene salvata all'interno di un database SQLlite insieme ad altre informazioni quali dispositivo utilizzato, versione di android installata,
tempo impiegato per la detection e il nome dell'immagine utilizzata.
Entrambi i task possono essere fatti partire separatamente e vengono eseguiti in un processo asincrono in background.
Il database può essere successivamente esportato e condiviso, ad esempio tramite email, in formato CSV.

## YoloV3

I pesi della rete neurale di YoloV3 vengono scaricati al primo avvio dell'applicativo ed è in grado di riconoscere oltre 80 animali/oggetti di uso comune.
Qui il [link al progetto](https://pjreddie.com/darknet/yolo/).

## MobileNetSSD

I pesi di questa rete sono già scaricati all'interno del progetto e non necessita di un download separato.
Si tratta di una rete più veloce ma meno accurata rispetto a YoloV3 e che riconosce poco più di 20 animali/oggetti di uso comune.
Qui il [link al progetto](https://github.com/chuanqi305/MobileNet-SSD).

## Alcuni screenshot

![Homepage](screenshot/homepage.jpg "Homepage")
![Menù](screenshot/menu.jpg "Menù")
![Object detection](screenshot/Object detection.jpg "Object detection")
![Email](screenshot/email.jpg "Email")

