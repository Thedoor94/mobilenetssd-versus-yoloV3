package advmobdev.unipr.it.mobilenetssdvsyolov3;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.opencsv.CSVWriter;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.dnn.Dnn;
import org.opencv.dnn.Net;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {


    private static final int SELECT_PICTURE = 1;

    private static final int GALLERY = 1;
    private static final int SSD = 2;
    private static final int YOLOV3 = 3;

    private float confThreshold = 0.5f;

    private String selectedImagePath;
    private File folder;
    private Net net;
    private boolean ssd_on = false;
    private boolean yolo_on = false;
    private boolean presence_image = false;
    private boolean ssd_not_found = false;
    private boolean yolo_not_found= false;

    private String proto;
    private String weights_ssd;

    private String cfg;
    private String weights_yolo;

    public static final int WRITE_STORAGE_PERMISSION_REQUEST_CODE = 1;
    private Mat sampledImage = null;
    private Mat originalImage = null;
    private String imageName;

    private List<String> names;

    private ImageView imageView_ssd;
    private ImageView imageView_yoloV3;
    private TextView timeText_ssd;
    private TextView timeText_yolo;
    private Button yoloButton;
    private Button ssdButton;
    private ProgressDialog pDialog;

    private double time_yolo = 0;
    private double time_ssd = 0;

    private static final String[] classNames = {"background",
            "aeroplane", "bicycle", "bird", "boat",
            "bottle", "bus", "car", "cat", "chair",
            "cow", "diningtable", "dog", "horse",
            "motorbike", "person", "pottedplant",
            "sheep", "sofa", "train", "tvmonitor" };

    // openCV initialization
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                    Log.i("OpenCV", "OpenCV loaded successfully");
                    break;
                default:
                    super.onManagerConnected(status);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("OpenCV", "called onCreate");

        //check permission
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    WRITE_STORAGE_PERMISSION_REQUEST_CODE);
        }

        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);
        imageView_ssd = (ImageView) findViewById(R.id.imageView_ssd);
        imageView_yoloV3 = (ImageView) findViewById(R.id.imageView_yolo);
        timeText_ssd = (TextView) findViewById(R.id.textTimeSSD);
        timeText_yolo = (TextView) findViewById(R.id.textTimeYolo);
        yoloButton = (Button) findViewById(R.id.YoloButton);
        ssdButton = (Button) findViewById(R.id.SSD_Button);

        proto = getPath_SSD("MobileNetSSD_deploy.prototxt", this);
        weights_ssd = getPath_SSD("MobileNetSSD_deploy.caffemodel", this);

        cfg = getPath_SSD("cfg_yolov3.cfg", this);

        folder = new File(Environment.getExternalStorageDirectory() + "/YoloV3Weights");
        if (!folder.exists()) {
            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
            alertDialog.setTitle("Download");
            alertDialog.setMessage(MainActivity.this.getString(R.string.download));

            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            folder.mkdir();
                            new DownloadFileFromURL().execute("https://pjreddie.com/media/files/yolov3.weights");
                        }
                    });
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }

        weights_yolo = Environment.getExternalStorageDirectory().toString() + "/YoloV3Weights/yolov3.weights";
        names = new ArrayList<>();
        names = readCoco(names);

        yoloButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (yolo_on && ssd_on) {
                    insertIntoDB("YOLOV3", time_yolo,time_ssd, imageName);
                    Toast.makeText(getApplicationContext(), "Preference added to database", Toast.LENGTH_LONG).show();
                    deactivateButton();
                }
            }
        });

        ssdButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (yolo_on && ssd_on) {
                    insertIntoDB("SSD", time_yolo, time_ssd, imageName);
                    Toast.makeText(getApplicationContext(), "Preference added to database", Toast.LENGTH_LONG).show();
                    deactivateButton();
                }
            }
        });

    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            System.out.println("Starting download");

            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Loading... Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {

                File folder = new File(Environment.getExternalStorageDirectory()
                        + "/YoloV3Weights/");

                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();
                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream
                OutputStream output = new FileOutputStream(folder.toString() + "/YoloV3.weights");

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            pDialog.dismiss();
            Toast.makeText(getApplicationContext(), "YoloV3.weights" + " downloaded.", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        // Calls the asynchronous initialization and passes the callback object
        // created beforehand, and chooses which version of OpenCV to load
        // It also serves to verify that the installed OpenCV manager
        // supports the version you are trying to upload.

        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, this,
                mLoaderCallback);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_openGallery) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_PICK);
            startActivityForResult(Intent.createChooser(intent,
                    "Seleziona immagine"), SELECT_PICTURE);
            return true;
        }
        if (id == R.id.action_MobileNetSSD && presence_image) {
            timeText_ssd.setText("Loading...");
            timeText_ssd.setVisibility(View.VISIBLE);
            mobileNetSSD(sampledImage);
        }
        if (id == R.id.action_YoloV3 && presence_image) {
            timeText_yolo.setText("Loading...");
            timeText_yolo.setVisibility(View.VISIBLE);
            yoloV3(sampledImage);
        }
        if (id == R.id.action_sendDB) {

            exportDB();
            Cursor c = readDB();
            if (c.getCount() !=0) {
                Intent myIntent = new Intent(getBaseContext(), SendEmail.class);
                startActivity(myIntent);
            } else {
                Toast.makeText(getApplicationContext(), "Your database is empty", Toast.LENGTH_LONG).show();
            }
        }

        if (id== R.id.action_emptyDB) {
            deleteRowsDB();
            Toast.makeText(getApplicationContext(), "Now your database is empty", Toast.LENGTH_LONG).show();
        }

        return super.onOptionsItemSelected(item);
    }


    public void onActivityResult(int requestCode, int resultCode,
                                 Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                selectedImagePath = getPath(selectedImageUri);
                loadImage(selectedImagePath);
                displayImage(sampledImage, GALLERY);
                yolo_on = false;
                ssd_on = false;
            }
        }
    }

    private String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }
        // Try to get the image from Media Store
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection,
                null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(
                    MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    private void loadImage(String path) {

        originalImage = Imgcodecs.imread(path);

        File f = new File(path);
        imageName = f.getName();

        Mat rgbImage = new Mat();
        Imgproc.cvtColor(originalImage, rgbImage, Imgproc.COLOR_BGR2RGB);

        int reqWidth = imageView_yoloV3.getWidth();
        int reqHeight = imageView_yoloV3.getHeight();

        presence_image = true;    //flag per verificare presenza immagine su schermo

        try {
            ExifInterface exif = new ExifInterface(selectedImagePath);
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 1);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    // ottieni l'immagine specchiata
                    rgbImage = rgbImage.t();
                    // flip lungo l'asse y
                    Core.flip(rgbImage, rgbImage, 1);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    // ottieni l'immagine "sotto-sopra"
                    rgbImage = rgbImage.t();
                    // flip lungo l'asse x
                    Core.flip(rgbImage, rgbImage, 0);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        sampledImage = new Mat();

        int height = rgbImage.height();
        int width = rgbImage.width();

        double heightRatio = 1;
        double widthRatio = 1;

        if (height > reqHeight || width > reqWidth) {
            // Calcoliamo i rapporti tra altezza e larghezza richiesti e quelli dell 'immagine sorgente
            heightRatio = (double) reqHeight / (double) height;
            widthRatio = (double) reqWidth / (double) width;
        }

        Imgproc.resize(rgbImage, sampledImage, new Size(), widthRatio,
                heightRatio, Imgproc.INTER_AREA);

        //reset inference time and disable vote button
        time_ssd = 0;
        time_yolo = 0;
        timeText_ssd.setVisibility(View.INVISIBLE);
        timeText_yolo.setVisibility(View.INVISIBLE);
        deactivateButton();
    }

    private void displayImage(Mat image, int type) {
        // Create a Bitmap
        Bitmap bitMap = Bitmap.createBitmap(image.cols(), image.rows(),
                Bitmap.Config.RGB_565);
        // Conver image from Mat to Bitmap
        Utils.matToBitmap(image, bitMap);

        if (type == GALLERY) {
            imageView_ssd.setImageBitmap(bitMap);
            imageView_yoloV3.setImageBitmap(bitMap);
        } else if (type == SSD) {
            imageView_ssd.setImageBitmap(bitMap);
        } else {
            imageView_yoloV3.setImageBitmap(bitMap);
        }
    }

    private void mobileNetSSD(Mat image) {

        if (!ssd_on) {
            new MobileNetSSDAsyncTask().execute(image);
        }
        ssd_on = true;
    }


    private void yoloV3(Mat image) {

        if (!yolo_on) {
            new YoloAsyncTask().execute(image);
        }
        yolo_on = true;
    }

    private static List<String> getOutputNames(Net net) {
        List<String> names = new ArrayList<>();

        List<Integer> outLayers = net.getUnconnectedOutLayers().toList();
        List<String> layersNames = net.getLayerNames();

        outLayers.forEach((item) -> names.add(layersNames.get(item - 1)));
        return names;
    }

    private List<String> readCoco(List<String> names) {

        AssetManager am = this.getAssets();
        try {
            InputStream is = am.open("coco.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            while (reader.ready()) {
                names.add(reader.readLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return names;
    }

    private static String getPath_SSD(String file, Context context) {
        AssetManager assetManager = context.getAssets();
        BufferedInputStream inputStream = null;
        try {
            // Read data from assets.
            inputStream = new BufferedInputStream(assetManager.open(file));
            byte[] data = new byte[inputStream.available()];
            inputStream.read(data);
            inputStream.close();
            // Create copy file in storage.
            File outFile = new File(context.getFilesDir(), file);
            FileOutputStream os = new FileOutputStream(outFile);
            os.write(data);
            os.close();
            // Return a path to file which may be read in common way.
            return outFile.getAbsolutePath();
        } catch (IOException ex) {
            Log.i("File Error", "Failed to upload a file");
        }
        return "";
    }

    public class YoloAsyncTask extends AsyncTask<Mat, Mat, Double> {

        @Override
        protected Double doInBackground(Mat... mats) {

            Mat image = mats[0].clone();

            long startTime = System.currentTimeMillis();
            Net net = Dnn.readNetFromDarknet(cfg, weights_yolo);

            Mat blob = Dnn.blobFromImage(image, 0.00392, new Size(416, 416), new Scalar(0), true, false);
            net.setInput(blob);

            List<Mat> result = new ArrayList<>();

            for (int k = 0; k < getOutputNames(net).size(); ++k) {
                result.add(net.forward(getOutputNames(net).get(k)));
            }

            List<Integer> clsIds = new ArrayList<>();
            List<Float> confs = new ArrayList<>();
            List<Rect> rects = new ArrayList<>();

            for (int i = 0; i < result.size(); ++i) {
                // each row is a candidate detection, the 1st 4 numbers are
                // [center_x, center_y, width, height], followed by (N-4) class probabilities
                Mat level = result.get(i);
                for (int j = 0; j < level.rows(); ++j) {
                    Mat row = level.row(j);
                    Mat scores = row.colRange(5, level.cols());
                    Core.MinMaxLocResult mm = Core.minMaxLoc(scores);
                    float confidence = (float) mm.maxVal;

                    org.opencv.core.Point classIdPoint = mm.maxLoc;

                    if (confidence > confThreshold) {
                        int centerX = (int) (row.get(0, 0)[0] * image.cols());
                        int centerY = (int) (row.get(0, 1)[0] * image.rows());
                        int width = (int) (row.get(0, 2)[0] * image.cols());
                        int height = (int) (row.get(0, 3)[0] * image.rows());
                        int left = centerX - width / 2;
                        int top = centerY - height / 2;

                        confs.add(confidence);
                        clsIds.add((int) classIdPoint.x);
                        rects.add(new Rect(left, top, width, height));

                    }
                }
            }
            if (!rects.isEmpty()) {
                yolo_not_found=false;
                non_maximum_suppression(image, confs, rects, clsIds);
            } else {
                displayImage(image, YOLOV3);
                yolo_not_found=true;
            }

            long stopTime = System.currentTimeMillis();
            time_yolo = (double) ((stopTime - startTime));

            return time_yolo;
        }

        protected void onPostExecute(Double result) {
            if (!yolo_not_found) {
                writeTime(YOLOV3, result);
                activateButton();
            } else {
                Toast.makeText(getApplicationContext(), "Yolo did not detect any objects", Toast.LENGTH_LONG).show();
            }
        }

    }

    private void non_maximum_suppression(Mat image, List<Float> confs, List<Rect> rects, List<Integer> clsIds) {

        float nmsThresh = 0.5f;
        MatOfFloat confidences = new MatOfFloat();
        confidences.fromList(confs);
        Rect[] boxesArray = rects.toArray(new Rect[0]);
        MatOfRect boxes = new MatOfRect(boxesArray);
        MatOfInt indices = new MatOfInt();
        Dnn.NMSBoxes(boxes, confidences, confThreshold, nmsThresh, indices);

        int[] ind = indices.toArray();
        for (int i = 0; i < ind.length; ++i) {
            int idx = ind[i];
            Rect box = boxesArray[idx];
            Imgproc.rectangle(image, box.tl(), box.br(), new Scalar(0, 255, 0), 2);

            String confidence_string = String.format("%.3f", confs.get(idx));
            String label = names.get(clsIds.get(idx)) + ": " + confidence_string;
            int[] baseLine = new int[1];
            Size labelSize = Imgproc.getTextSize(label, Core.FONT_HERSHEY_SIMPLEX, 0.5, 1, baseLine);
            Imgproc.rectangle(image, new org.opencv.core.Point(box.x, box.y+baseLine[0] + labelSize.height),
                    new org.opencv.core.Point(box.x + labelSize.width, box.y),
                    new Scalar(255, 255, 255), Core.FILLED);
            // Write class name and confidence.
            Imgproc.putText(image, label, new org.opencv.core.Point(box.x, box.y + labelSize.height),
                    Core.FONT_HERSHEY_SIMPLEX, 0.5, new Scalar(0, 0, 0));
        }

        displayImage(image, YOLOV3);
    }

    public class MobileNetSSDAsyncTask extends AsyncTask<Mat, Mat, Double> {
        @Override
        protected Double doInBackground(Mat... mats) {

            Mat cloneImage = mats[0].clone();

            long startTime = System.currentTimeMillis();

            final double IN_WIDTH = 300.0;
            final double IN_HEIGHT = 300.0;
            final double IN_SCALE_FACTOR = 0.007843;
            final double MEAN_VAL = 127.5;
            final double THRESHOLD = 0.5;

            net = Dnn.readNetFromCaffe(proto, weights_ssd);

            // Forward image through network.
            Mat blob = Dnn.blobFromImage(cloneImage, IN_SCALE_FACTOR,
                    new Size(IN_WIDTH, IN_HEIGHT),
                    new Scalar(MEAN_VAL, MEAN_VAL, MEAN_VAL), false);

            net.setInput(blob);
            Mat detections = net.forward();

            detections = detections.reshape(1, (int) detections.total() / 7);

            for (int i = 0; i < detections.rows(); ++i) {

                double confidence = detections.get(i, 2)[0];
                String confidence_string = String.format("%.3f", confidence);

                if (confidence > THRESHOLD) {

                    int classId = (int) detections.get(i, 1)[0];
                    int left = (int) (detections.get(i, 3)[0] * cloneImage.cols());
                    int top = (int) (detections.get(i, 4)[0] * cloneImage.rows());
                    int right = (int) (detections.get(i, 5)[0] * cloneImage.cols());
                    int bottom = (int) (detections.get(i, 6)[0] * cloneImage.rows());

                    // Draw rectangle around detected object.
                    Imgproc.rectangle(cloneImage, new org.opencv.core.Point(left, top), new org.opencv.core.Point(right, bottom),
                            new Scalar(0, 0, 255), 2);
                    String label = classNames[classId] + ": " + confidence_string;
                    int[] baseLine = new int[1];
                    Size labelSize = Imgproc.getTextSize(label, Core.FONT_HERSHEY_SIMPLEX, 0.5, 1, baseLine);
                    // Draw background for label.
                    Imgproc.rectangle(cloneImage, new org.opencv.core.Point(left, top +baseLine[0] + labelSize.height),
                            new org.opencv.core.Point(left + labelSize.width, top),
                            new Scalar(255, 255, 255), Core.FILLED);
                    // Write class name and confidence.
                    Imgproc.putText(cloneImage, label, new org.opencv.core.Point(left, top+ labelSize.height),
                            Core.FONT_HERSHEY_SIMPLEX, 0.5, new Scalar(0, 0, 0));
                }
            }
            displayImage(cloneImage, SSD);

            if (detections.rows()==0) {
                ssd_not_found=true;
            } else {
                ssd_not_found=false;
            }

            long stopTime = System.currentTimeMillis();
            time_ssd = (double) ((stopTime - startTime));

            return time_ssd;
        }

        protected void onPostExecute(Double result) {

            if (!ssd_not_found) {
                writeTime(SSD, result);
                activateButton();
            } else {
                Toast.makeText(getApplicationContext(), "MobileNet SSD did not detect any objects", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void insertIntoDB(String preference, Double timeYolo, Double timeSSD, String imageId) {
        DBManager db = new DBManager(this);
        db.open();
        db.insertInfo(preference, timeYolo, timeSSD, imageId);
        db.close();
    }

    private void deleteRowsDB () {
        DBManager db = new DBManager(this);
        db.open();
        db.delete_rows();
        db.close();
    }

    private Cursor readDB() {
        DBManager db = new DBManager(this);
        db.open();
        Cursor c = db.getData();
        db.close();
        return c;
    }

    private void exportDB() {

        DBhelper dbhelper = new DBhelper(getApplicationContext());
        File exportDir = new File(Environment.getExternalStorageDirectory(), "");
        if (!exportDir.exists())
        {
            exportDir.mkdirs();
        }

        File file = new File(exportDir, "PreferencesComparison.csv");
        try
        {
            file.createNewFile();
            CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
            SQLiteDatabase db = dbhelper.getReadableDatabase();

            Cursor curCSV = db.rawQuery("SELECT * FROM PreferenceTable",null);
            csvWrite.writeNext(curCSV.getColumnNames());

            while(curCSV.moveToNext())
            {
                String arrStr[] ={curCSV.getString(0),curCSV.getString(1), ""+curCSV.getFloat(2),
                        curCSV.getString(3),curCSV.getString(4), curCSV.getString(5),curCSV.getString(6)};
                csvWrite.writeNext(arrStr);
            }
            csvWrite.close();
            curCSV.close();
        }
        catch(Exception sqlEx)
        {
            Log.e("MainActivity", sqlEx.getMessage(), sqlEx);
        }
    }

    private void writeTime(int type, double time) {
        if (type == SSD) {
            timeText_ssd.setText("Execution time: " + time + " millisec");

        } else {
            timeText_yolo.setText("Execution time: " + time + " millisec");
        }
    }

    private void activateButton() {

        if (ssd_on && yolo_on) {
            yoloButton.setEnabled(true);
            ssdButton.setEnabled(true);
        }
    }

    private void deactivateButton() {

        yoloButton.setEnabled(false);
        ssdButton.setEnabled(false);

    }
}