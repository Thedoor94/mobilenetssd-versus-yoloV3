package advmobdev.unipr.it.mobilenetssdvsyolov3;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.File;

public class SendEmail extends AppCompatActivity {

    private Button emailButton;
    private EditText email;
    private String address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_email);

        emailButton = (Button) findViewById(R.id.button_send_email);
        email = (EditText) findViewById(R.id.email_edit_text);

        email.requestFocus();

        emailButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                address = email.getText().toString();
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                if (!address.isEmpty()) {
                    send_email();
                } else {
                    Toast.makeText(getApplicationContext(), "You must first enter a valid email address", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void send_email() {
        String filename = "PreferencesComparison.csv";
        File filelocation = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), filename);
        Uri path = Uri.fromFile(filelocation);
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        // set the type to 'email'
        emailIntent.setType("vnd.android.cursor.dir/email");
        String to[] = {address};
        emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
        // the attachment
        emailIntent.putExtra(Intent.EXTRA_STREAM, path);
        // the mail subject
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "MobileNetSSD vs YoloV3");
        startActivity(Intent.createChooser(emailIntent, "Send mail..."));

    }
}
