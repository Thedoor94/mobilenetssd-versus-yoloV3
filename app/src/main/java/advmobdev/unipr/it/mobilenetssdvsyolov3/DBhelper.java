package advmobdev.unipr.it.mobilenetssdvsyolov3;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBhelper extends SQLiteOpenHelper {

    public static final String DBNAME = "PreferenceDB";
    public static final String TBL_NAME = "PreferenceTable";
    public static final String FIELD_ID = "_id";
    public static final String FIELD_PREFERENCE = "preferenza";
    public static final String FIELD_TIME_YOLO = "tempo_YoloV3";
    public static final String FIELD_TIME_SSD = "tempo_SSD";
    public static final String IMAGE_ID = "imageId";
    public static final String SMARTPHONE_MODEL = "dispositivo";
    public static final String ANDROID_VERSION = "versione_android";


    public DBhelper(Context context) {
        super(context, DBNAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String q = "CREATE TABLE " + TBL_NAME +
                " ( _id INTEGER PRIMARY KEY AUTOINCREMENT," +
                FIELD_PREFERENCE + " TEXT," +
                FIELD_TIME_YOLO + " REAL," +
                FIELD_TIME_SSD + " REAL," +
                IMAGE_ID + " TEXT,"+
                SMARTPHONE_MODEL + " TEXT,"+
                ANDROID_VERSION + " TEXT)";
        db.execSQL(q);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(DBhelper.class.getName(),"Aggiornamento database dalla versione " + oldVersion + " alla "
                + newVersion + ". I dati esistenti verranno eliminati.");
        db.execSQL("DROP TABLE IF EXISTS PreferencesTable");
        onCreate(db);
    }

}
