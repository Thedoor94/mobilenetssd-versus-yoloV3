package advmobdev.unipr.it.mobilenetssdvsyolov3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;

public class DBManager {

    final Context context;
    DBhelper dbhelper;
    SQLiteDatabase db;

    public DBManager(Context ctx)
    {
        this.context = ctx;
        dbhelper = new DBhelper(context);
    }

    public DBManager open() throws SQLException
    {
        db = dbhelper.getWritableDatabase();
        return this;
    }


    public void close()
    {
        dbhelper.close();
    }

    public long insertInfo(String preference, Double timeYolo,Double timeSSD, String imageId)
    {
        ContentValues initialValues = new ContentValues();
        initialValues.put(dbhelper.FIELD_PREFERENCE, preference);
        initialValues.put(dbhelper.FIELD_TIME_YOLO, timeYolo);
        initialValues.put(dbhelper.FIELD_TIME_SSD, timeSSD);
        initialValues.put(dbhelper.IMAGE_ID, imageId);
        initialValues.put(dbhelper.SMARTPHONE_MODEL, Build.MANUFACTURER+" "+ Build.MODEL);
        initialValues.put(dbhelper.ANDROID_VERSION, Build.VERSION.RELEASE);

        return db.insert(dbhelper.TBL_NAME, null, initialValues);
    }

    public Cursor getData() throws SQLException
    {
        Cursor mCursore = db.query(true, dbhelper.TBL_NAME, new String[] {dbhelper.FIELD_ID,dbhelper.FIELD_PREFERENCE, dbhelper.FIELD_TIME_YOLO, dbhelper.FIELD_TIME_SSD,
                dbhelper.IMAGE_ID, dbhelper.SMARTPHONE_MODEL, dbhelper.ANDROID_VERSION}, null, null, null, null, null, null);
        if (mCursore != null) {
            mCursore.moveToFirst();
        }
        return mCursore;
    }

    public void erase_db () {
        db = dbhelper.getWritableDatabase();
        dbhelper.onUpgrade(db,3,4);
    }

    public void delete_rows () {
        db.execSQL("delete from "+ dbhelper.TBL_NAME);
    }

}
